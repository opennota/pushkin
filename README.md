pushkin [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![GoDoc](http://godoc.org/gitlab.com/opennota/pushkin?status.svg)](http://godoc.org/gitlab.com/opennota/pushkin) [![Pipeline status](https://gitlab.com/opennota/pushkin/badges/master/pipeline.svg)](https://gitlab.com/opennota/pushkin/commits/master)
=======

Search poems and pieces by Alexander Sergeyevich Pushkin with the help of [word2vec](https://en.wikipedia.org/wiki/Word2vec).

![Screencast](/screencast.gif)

## Install

    go get -u gitlab.com/opennota/pushkin

Or download a pre-built version from the [Releases](https://github.com/opennota/pushkin/releases) page.

## Use

Download a word2vec model from [http://rusvectores.org/ru/models](http://rusvectores.org/ru/models/). E.g., ruscorpora. Extract it with gunzip.

Then

    pushkin ruscorpora_1_300_10.bin pushkin.json-stream

## Donate

**Bitcoin (BTC):** `1PEaahXKwJvNJGJa2PXtPFLNYYigmdLXct`

**Ethereum (ETH):** `0x83e9607E693467Cb344244Df10f66c036eC3Dc53`
